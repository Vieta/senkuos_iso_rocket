# SenkuOS-ISO Rocket edition
The SenkuOS Rocket edition uses the Calamares ISO from EndeavourOS for the install.

In the near future it will have other Arch Linux based repositories included.


# How to build ISO

You need to use an installed any archbased system with EndeavourOS [repository](https://github.com/endeavouros-team/mirrors) enabled.

As the installer packages and needed dependencies will get installed from EndeavourOS repository.

general information: 

https://endeavouros-team.github.io/EndeavourOS-Development/

And read the changelog before starting to know about latest changes:

https://github.com/endeavouros-team/EndeavourOS-ISO/blob/main/CHANGELOG.md

### Install build dependencies

```
sudo pacman -S archiso mkinitcpio-archiso git squashfs-tools --needed
```
Recommended to reboot after this changes.

### Build

##### 1. Prepare

If you want the last release state to rebuild ISO you need to use specific tag tarball.
https://github.com/endeavouros-team/EndeavourOS-ISO/tags

use latest main branch of EOS (development branch):

```
git clone https://github.com/endeavouros-team/EndeavourOS-ISO.git
cd "EndeavourOS-ISO"
./prepare.sh
```

##### 2. Build

~~~
sudo ./mkarchiso -v "."
~~~

**or with log:**

~~~
sudo ./mkarchiso -v "." 2>&1 | tee "eosiso_$(date -u +'%Y.%m.%d-%H:%M').log"
~~~

##### 3. The .iso appears in `out` directory


## Advanced

To install locally builded packages on ISO put the packages inside directory:

~~~
airootfs/root/packages
~~~

Packages will get installed and directory will be cleaned up after that.
